---

  pw_hash: $6$SEaggDjiCCX4Togw$Ywp6LEk0MCA1Axl7xQj78CXCOH2JDn1YAahqqZ4DH21GMZueTqT5LisIaCzES.0UjZfUFg/qngHgtum2INXNH1
  # To generate a password hash:
  # python3 -c 'import crypt; import os; print(crypt.crypt("password123", crypt.mksalt(crypt.METHOD_SHA512)))'

  authorized_keys: 
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4tzrayU6ahMhmWuicy+oFfy//9oB+2EdbbmDfA0d+k3SpYjWVqho64/L+sQIAN0RGBJx42GkbKi8B6AriPw8omLOCk2WSYW3ymEC7n3l32M5T4cLr8LIYwoMOBZkMtRc3H62PrHgDoTJLhUOvT2ewj1SLl7iU5gQuInwPE6jWooIb8R6KMUl31qNpkafCVPz5ovw0iYbDamHQF6sq081Xl39px2345T8TofIAocyBUfCOstmAvPaD9lXIV3j9JmPhAy0oweXpxdPiQzBHXepLh/jrvHrV5ggl2iwmLgF3uzwYdFlQN6eCniBtBEcGqEacb6oP2KHfHer04WIbAMHZ eduardoefb@efb
          
  libvirt:
    profiles:
      - name: prof01
        qemu_path: "/usr/bin/qemu-system-x86_64"
        cpu:
          mode: "custom"
          match: "exact"
          check: "full"
          model: "Westmere-IBRS"
          vendor: "Intel"
          features:
            - policy: require
              name: vme
            - policy: require
              name: pclmuldq
            - policy: require
              name: vmx
            - policy: require
              name: pcid
            - policy: require
              name: x2apic
            - policy: require
              name: tsc-deadline
            - policy: require
              name: hypervisor
            - policy: require
              name: arat
            - policy: require
              name: tsc_adjust
            - policy: require
              name: stibp
            - policy: require
              name: ssbd
            - policy: require
              name: pdpe1gb
            - policy: require 
              name: rdtscp
            - policy: require
              name: ibpb

  timezone: Brazil/East

  common:
    os_disk_size: "40G"

  hypervisors:  
    - name: srv02
      ip: 10.2.1.31
      workdir: /srv/kubespray  

    - name: srv03
      ip: 10.2.1.33
      workdir: /srv/kubespray    
        
  network:

    domain: cloud.int
    ntp_servers:      
      - 0.centos.pool.ntp.org
      - 1.centos.pool.ntp.org
      - 2.centos.pool.ntp.org
      - 3.centos.pool.ntp.org
      
    oam:
      name: magma_oam
      external_vlan: 20
      external_interface: eth3
      network: 172.31.10.0
      broadcast: 172.31.10.31
      gateway: 172.31.10.1
      netmask: 255.255.255.224
      netmask_len: 27
      dns: 8.8.8.8
      dev: "vnet0"
      alias: "net0"
      slot: "0x10"
      type: "e1000"
      mtu: 1500
 
    external: 
      - name: magma_s1
        external_vlan: 21
        external_interface: eth3
        network: 172.31.10.32      
        broadcast: 172.31.10.47      
        netmask: 255.255.255.240
        netmask_len: 28
        dev: "vnet1"
        alias: "net1"
        slot: "0x11" 
        type: "e1000"   
        mtu: 1500


  virtual_router:
    hypervisor: "{{ hypervisors[0] }}"
    masquerade:
      - eth0
    name: kubespray_router
    interfaces:
      - name: ext_magma_0
        ip: 172.31.10.253/30
        master: None
        peer: 
          name: ext_magma_1
          ip: 172.31.10.254/30
      - name: oam_magma_0
        ip: None
        master: "{{ network.oam.name }}"
        peer: 
          name: oam_magma_1
          ip: 172.31.10.1/28
      - name: magma_s1_0
        ip: None
        master: "{{ network.external[0].name }}"
        peer: 
          name: magma_s1_1
          ip: 172.31.10.33/28          
 

    external_routes:
      - network: 172.31.10.0/26
        gw: 172.31.10.254

    internal_routes:
      - network: default
        gw: 172.31.10.253
    
  nodes:
    - name: compute1
      roles: 
        - k8s_master
        - k8s_worker
        - master01
        - master
        - worker

      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "compute1_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.5"  
      oam_mac: "00:60:2f:37:B4:30"          
      external_ips: []   
      vnc: 
        port: 12000 

    - name: compute2
      roles: 
        - k8s_master
        - k8s_worker
        - master02
        - master
        - worker

      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "compute2_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.6"  
      oam_mac: "00:60:2f:37:B4:31"          
      external_ips: []   
      vnc: 
        port: 12001      

    - name: compute3
      roles: 
        - k8s_master
        - k8s_worker
        - master03
        - master
        - worker

      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "compute3_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.7"  
      oam_mac: "00:60:2f:37:B4:32"          
      external_ips: []   
      vnc: 
        port: 12002        

    - name: aagw01
      roles: 
        - agw

        
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "aagw01_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.8"  
      oam_mac: "00:60:2f:37:B4:33"          
      external_ips: 
        - 172.31.10.36

      vnc: 
        port: 12003         


    - name: nfsreg01
      roles: 
        - nfs 
        - dns   
        - nfs01 
        - deployment

      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[0] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "nfsreg01_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.9"  
      oam_mac: "00:60:2f:37:B4:35"          
      external_ips: []   
      vnc: 
        port: 12004 

    - name: nfsreg02
      roles: 
        - nfs 
        - dns    
        - nfs02

      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "nfsreg02_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "172.31.10.10"  
      oam_mac: "00:60:2f:37:B4:36"          
      external_ips: []   
      vnc: 
        port: 12005       
       
        
                    