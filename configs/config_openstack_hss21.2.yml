---

  pw_hash: $6$SEaggDjiCCX4Togw$Ywp6LEk0MCA1Axl7xQj78CXCOH2JDn1YAahqqZ4DH21GMZueTqT5LisIaCzES.0UjZfUFg/qngHgtum2INXNH1
  # To generate a password hash:
  # python3 -c 'import crypt; import os; print(crypt.crypt("password123", crypt.mksalt(crypt.METHOD_SHA512)))'

  authorized_keys: 
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4tzrayU6ahMhmWuicy+oFfy//9oB+2EdbbmDfA0d+k3SpYjWVqho64/L+sQIAN0RGBJx42GkbKi8B6AriPw8omLOCk2WSYW3ymEC7n3l32M5T4cLr8LIYwoMOBZkMtRc3H62PrHgDoTJLhUOvT2ewj1SLl7iU5gQuInwPE6jWooIb8R6KMUl31qNpkafCVPz5ovw0iYbDamHQF6sq081Xl39px2345T8TofIAocyBUfCOstmAvPaD9lXIV3j9JmPhAy0oweXpxdPiQzBHXepLh/jrvHrV5ggl2iwmLgF3uzwYdFlQN6eCniBtBEcGqEacb6oP2KHfHer04WIbAMHZ eduardoefb@efb
          
  libvirt:
    profiles:
      - name: prof01
        qemu_path: "/usr/bin/qemu-system-x86_64"
        cpu:
          mode: "custom"
          match: "exact"
          check: "full"
          model: "Westmere-IBRS"
          vendor: "Intel"
          features:
            - policy: require
              name: vme
            - policy: require
              name: pclmuldq
            - policy: require
              name: vmx
            - policy: require
              name: pcid
            - policy: require
              name: x2apic
            - policy: require
              name: tsc-deadline
            - policy: require
              name: hypervisor
            - policy: require
              name: arat
            - policy: require
              name: tsc_adjust
            - policy: require
              name: stibp
            - policy: require
              name: ssbd
            - policy: require
              name: pdpe1gb
            - policy: require 
              name: rdtscp
            - policy: require
              name: ibpb

  timezone: Brazil/East
  hypervisors:  
    - name: srv02
      ip: 10.2.1.31
      workdir: /srv/hss_21_2  

    - name: srv03
      ip: 10.2.1.33
      workdir: /srv/hss_21_2  
        
  network:

    domain: cloud.int
    ntp_servers:      
      - 0.centos.pool.ntp.org
      - 1.centos.pool.ntp.org
      - 2.centos.pool.ntp.org
      - 3.centos.pool.ntp.org
      
    oam:
      name: lab_oam
      external_vlan: 10
      external_interface: eth3
      network: 10.6.0.0
      broadcast: 10.6.0.255
      gateway: 10.6.0.1
      netmask: 255.255.255.0
      netmask_len: 24
      dns: 8.8.8.8
      dev: "vnet0"
      alias: "net0"
      slot: "0x10"
      type: "e1000"
      mtu: 1500
 
    external: 
      - name: extnet01
        external_vlan: 11
        external_interface: eth3
        network: 10.7.0.0       
        broadcast: 10.7.0.255       
        netmask: 255.255.255.0
        netmask_len: 24
        dev: "vnet1"
        alias: "net1"
        slot: "0x11" 
        type: "e1000"   
        mtu: 1500

      - name: extnet02
        external_vlan: 12
        external_interface: eth3
        network: 10.8.0.0       
        broadcast: 10.8.0.255       
        netmask: 255.255.255.0
        netmask_len: 24
        dev: "vnet2"
        alias: "net2"
        slot: "0x12" 
        type: "e1000"   
        mtu: 1500        
      
  virtual_router:
    hypervisor: "{{ hypervisors[0] }}"
    masquerade: []
    
    name: hss20_router
    interfaces:
      - name: ext_vnet0
        ip: 10.10.10.5/30
        master: None
        peer: 
          name: ext_vnet1
          ip: 10.10.10.6/30
      - name: oam_vnet0
        ip: None
        master: "{{ network.oam.name }}"
        peer: 
          name: oam_vnet1
          ip: 10.6.0.1/24
      - name: extnet01_vnet0
        ip: None
        master: "{{ network.external[0].name }}"
        peer: 
          name: extnet01_vnet1
          ip: 10.7.0.1/24          
      - name: extnet02_vnet0
        ip: None
        master: "{{ network.external[1].name }}"
        peer: 
          name: extnet02_vnet1
          ip: 10.8.0.1/24      

    external_routes:
      - network: 10.6.0.0/24
        gw: 10.10.10.6
      - network: 10.7.0.0/24
        gw: 10.10.10.6
      - network: 10.8.0.0/24
        gw: 10.10.10.6

    internal_routes:
      - network: default
        gw: 10.10.10.5


  common:
    os_disk_size: "40G"
          
  nodes:
    - name: controller01
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[0] }}"
      ram: 16384000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "controller01_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "10.6.0.10"  
      oam_mac: "74:86:a1:c5:f9:ab"          
      external_ips:
        - "10.7.0.10"
        - "10.8.0.10"
      
      vnc: 
        port: 11110 


    - name: compute01
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 65536000
      cpus: 12   
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "compute01_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"        
      oam_ip: "10.6.0.20"  
      oam_mac: "74:86:a1:c5:f9:bb"          
      external_ips:
        - "10.7.0.20"
        - "10.8.0.20"
      
      vnc: 
        port: 11120 


    - name: compute02
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[0] }}"
      ram: 65536000
      cpus: 12 
      virt_profile: "{{ libvirt.profiles[0] }}"   
      disk: 
        - name: "compute02_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "10.6.0.21"  
      oam_mac: "74:86:a1:c5:f9:bc"          
      external_ips:
        - "10.7.0.21"
        - "10.8.0.21"
      
      vnc: 
        port: 11121  


    - name: compute03
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 65536000
      cpus: 12
      virt_profile: "{{ libvirt.profiles[0] }}" 
      disk: 
        - name: "compute03_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "10.6.0.22"  
      oam_mac: "74:86:a1:c5:f9:bd"          
      external_ips:
        - "10.7.0.22"
        - "10.8.0.22"
      
      vnc: 
        port: 11122         

    - name: compute04
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[0] }}"
      ram: 65536000
      cpus: 12
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "compute04_disk0.qcow2"
          size: "1024G"
          dev: "sda"
          unit: 1
          bus: "sata"
      oam_ip: "10.6.0.23"  
      oam_mac: "74:81:a4:c5:f9:ba"          
      external_ips:
        - "10.7.0.23"
        - "10.8.0.23"
      
      vnc:   
        port: 11123       
    

    - name: storage01
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[1] }}"
      ram: 8192000
      cpus: 4
      virt_profile: "{{ libvirt.profiles[0] }}"   
      disk: 
        - name: "storage01_disk0.qcow2"
          size: "512G"
          dev: "sda"
          unit: 1
          bus: "sata"
        - name: "storage01_disk1.qcow2"
          size: "2048G"
          dev: "sdb"
          unit: 2
          bus: "sata"          
      oam_ip: "10.6.0.30"  
      oam_mac: "74:86:a1:c5:f9:ca"          
      external_ips:
        - "10.7.0.30"
        - "10.8.0.30"
      
      vnc: 
        port: 11130         


    - name: storage02
      image: "/srv/image/ubuntu_20.04.img"
      hypervisor: "{{ hypervisors[0] }}"
      ram: 8192000
      cpus: 4 
      virt_profile: "{{ libvirt.profiles[0] }}"
      disk: 
        - name: "storage02_disk0.qcow2"
          size: "512G"
          dev: "sda"
          unit: 1
          bus: "sata"
        - name: "storage02_disk1.qcow2"
          size: "2048G"
          dev: "sdb"
          unit: 2   
          bus: "sata"                 
      oam_ip: "10.6.0.31"  
      oam_mac: "74:86:a1:c5:f9:cb"          
      external_ips:
        - "10.7.0.31"
        - "10.8.0.31"
      
      vnc: 
        port: 11131

####### This variable/object is only needed for openstack. Not needed if you are only using libvirt enviroment
  openstack:                
    controller:
      ip: 10.6.0.10
      host: controllervip
      name: controllervip.openstack.int
      
    provider_networks:
      - name: extnet01
        device: eth1
        gateway: 10.7.0.1
        range_begin: 10.7.0.100
        range_end: 10.7.0.200

      - name: extnet02
        device: eth2
        gateway: 10.8.0.1
        range_begin: 10.8.0.100
        range_end: 10.8.0.200        
    
    cinder:
       volume_name: cinder_volumes
       volume_device: sdb   


       
        
                    
